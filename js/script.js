Panier = new Array();
Montres = new Array();
countId = 1;
IsPanier = false;

let Montre = class Montre
{
    constructor(nom, desc, prix, img)
    {
        this.Id = countId++;
        this.Nom = nom;
        this.Description = desc;
        this.Prix = prix;
        this.Img = img;
    }

    get id()
    {
        return this.Id;
    }
}


let m1 = new Montre("Cool watch","Une montre très bien",200,"./images/montre2.jpg");
Montres.push(m1);
let m2 = new Montre("Cool watch","Une montre très bien",250,"./images/montre3.jpg");
Montres.push(m2);
let m3 = new Montre("Cool watch","Une montre très bien",200,"./images/montre2.jpg");
Montres.push(m3);
let m4 = new Montre("Cool watch","Une montre très bien",200,"./images/montre2.jpg");
Montres.push(m4);
let m5 = new Montre("Cool watch","Une montre très bien",200,"./images/montre2.jpg");
Montres.push(m5);
let m6 = new Montre("Cool watch","Une montre très bien",200,"./images/montre2.jpg");
Montres.push(m6);
let m7 = new Montre("Cool watch","Une montre très bien",200,"./images/montre2.jpg");
Montres.push(m7);
let m8 = new Montre("Cool watch","Une montre très bien",200,"./images/montre2.jpg");
Montres.push(m8);
let m9 = new Montre("Cool watch","Une montre très bien",200,"./images/montre2.jpg");
Montres.push(m9);
let m10 = new Montre("Cool watch","Une montre très bien",200,"./images/montre2.jpg");
Montres.push(m10);

function ajoutPanier(_id)
{
    let montre = Montres.find(x => x.Id == _id);
    Panier.push(montre);

    if (IsPanier)
    {
        getToast();
    }
}

function closeToast()
{
    IsPanier = false;
}

function getToast()
{
    IsPanier = true;

    let newToast = document.createElement("div");
    newToast.setAttribute("class","toast");
    newToast.setAttribute("role","alert");
    newToast.setAttribute("aria-live","assertive");
    newToast.setAttribute("aria-atomic","true");
    
    let toastHeader = document.createElement("div");
    toastHeader.setAttribute("class","toast-header");
    
    let toastHeaderText = document.createElement("strong");
    toastHeaderText.setAttribute("class","mr-auto");
    toastHeaderText.textContent = "Votre panier";

    let toastHeaderDismiss = document.createElement("button");
    toastHeaderDismiss.setAttribute("type","button");
    toastHeaderDismiss.setAttribute("class","ml-2 mb-1 close");
    toastHeaderDismiss.setAttribute("data-dismiss","toast");
    toastHeaderDismiss.setAttribute("aria-label","Close");
    toastHeaderDismiss.setAttribute("onclick","closeToast()");

    toastDismissIcon = document.createElement("span");
    toastDismissIcon.setAttribute("aria-hidden","true");
    toastDismissIcon.textContent = "&times;";
    toastHeaderDismiss.appendChild(toastDismissIcon);

    toastHeader.appendChild(toastHeaderText);
    toastHeader.appendChild(toastHeaderDismiss);
    newToast.appendChild(toastHeader);

    let toastBody = document.createElement("div");
    toastBody.setAttribute("class","toast-body");

    if (Panier.length > 0)
    {
        Panier.forEach(montre => {
            let toastParagraph = document.createElement("p");
            toastParagraph.textContent="Montre : "+montre.Nom+"; Prix : "+montre.Prix;
            toastBody.appendChild(toastParagraph);
        });
    }
    else
    {
        let toastParagraph = document.createElement("p");
        toastParagraph.textContent="Votre panier est vide !";
        toastBody.appendChild(toastParagraph);
    }

    newToast.appendChild(toastBody);
    let header = document.querySelector("header");
    header.appendChild(newToast);

    console.log(newToast);
    console.log(Panier);
}

function getMontres()
{
    Montres.forEach(montre => {
        let newCard = document.createElement("div");
        newCard.setAttribute("class","card");
    
        let cardImg = document.createElement("img");
        cardImg.setAttribute("src",montre.Img);
        cardImg.setAttribute("class","card-img-top");
        newCard.appendChild(cardImg);

        let cardBody = document.createElement("div");
        cardBody.setAttribute("class","card-body");

        let cardTitle = document.createElement("h5");
        cardTitle.setAttribute("class","card-title");
        cardTitle.textContent=montre.Nom;
        cardBody.appendChild(cardTitle);

        let cardDescription = document.createElement("p");
        cardDescription.setAttribute("class","card-text");
        cardDescription.textContent=montre.Description;
        cardBody.appendChild(cardDescription);

        let cardPrice = document.createElement("p");
        cardPrice.textContent=montre.Prix;
        cardBody.appendChild(cardPrice);

        let cardButton = document.createElement("input");
        cardButton.setAttribute("type","button");
        cardButton.setAttribute("style","visibility:hidden");
        cardButton.setAttribute("value","Ajouter au panier");
        cardButton.setAttribute("class","hiddenButton");
        cardButton.setAttribute("onclick","ajoutPanier(this.name)");
        cardButton.setAttribute("name",montre.Id);
        cardBody.appendChild(cardButton);
        newCard.appendChild(cardBody);

        let container = document.getElementById("container");
        container.appendChild(newCard);
    });
}